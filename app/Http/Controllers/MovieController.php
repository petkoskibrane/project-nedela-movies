<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function showMovies(){

        $movies = [];
        return view('showmovies')->with(['movies' => $movies]);
    }

    public function showAddMovieForm(){
        return view('addMovieForm');
    }

    public function addMovie(Request $request){


        return redirect()->route('show-movies');
    }

    public function showDetails(Request $request){

        return view('moviedetails');
    }

    public function deleteMovie(Request $request){

        return redirect()->route('show-movies');
    }

    public function showEditMovieForm(Request $request){

        return view('editMovieForm');
    }

    public function editMovie(Request $request){



        return redirect()->route('show-movies');
    }


    public function searchMovie(Request $request){


        $movies = [];

        return view('showmovies')->with(['movies' => $movies]);

    }

    public function searchMovieByYear(Request $request){


        $movies = [];

        return view('showmovies')->with(['movies' => $movies]);

    }


}
