@extends('defaultLayout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Title</th>
                        <th>Option</th>
                    </tr>



                    </thead>
                    <tbody>

                        <tr>

                            <td class="col-md-1">ID</td>
                            <td class="col-md-7">Title</td>
                            <td class="col-md-4">
                                <div class="col-md-4">
                                    <a class="btn btn-primary" href="{{route('movie-details')}}">Details</a>
                                </div>
                                <div class="col-md-4">
                                    <a class="btn btn-primary" href="{{route('edit-movie-form')}}">Edit</a>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{route('delete')}}" method="post">
                                        {{csrf_field()}}
                                        <input hidden  value="delete" name="_method"/>
                                        <input class="btn btn-danger" type="submit" value="Delete"/>
                                    </form>

                                </div>


                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
