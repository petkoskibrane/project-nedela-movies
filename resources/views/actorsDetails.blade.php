@extends('defaultLayout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Name</h1>
                <h4>Year : 2018</h4>
                <h4>Directors : <a href="{{route('director-details')}}">Peter Jakson</a>,
                    <a href="{{route('director-details')}}">Peter Jakson</a>,
                    <a href="{{route('director-details')}}">Peter Jakson</a></h4>
                <h4>Actors : <a href="{{route('actor-details')}}">Leonardo Di Caprio</a>,
                    <a href="{{route('actor-details')}}">Leonardo Di Caprio</a>
                    <a href="{{route('actor-details')}}">Leonardo Di Caprio</a></h4>
                <h4>Movies : <a href="{{route('movie-details')}}">Lord of the rings</a>,
                    <a href="{{route('movie-details')}}">Lord of the rings</a>
                    <a href="{{route('movie-details')}}">Lord of the rings</a></h4>

                <a class="btn btn-primary" href="{{route('show-movies')}}">Back</a>
            </div>
            <div class="col-md-6">
                <img src="https://img.buzzfeed.com/buzzfeed-static/static/2015-04/17/18/campaign_images/webdr10/heres-what-the-cast-of-the-lord-of-the-rings-look-2-4942-1429309498-32_dblbig.jpg" height="500"/>
            </div>
        </div>
    </div>


@endsection