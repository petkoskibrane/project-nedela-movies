@extends('defaultLayout')
@section('content')

    <div class="container">
        <form action="{{route('edit-movie')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name="title" value=""/>
            </div>
            <div class="form-group">
                <select class="form-control" name="year">
                    <option>2018</option>
                </select>
            </div>
            <div class="form-group">
                <label>Director</label>
                <select class="form-control" name="director">
                    <option selected>Peter Jackson</option>
                    <option>Peter Jackson 2</option>
                    <option>Peter Jackson 3</option>
                    <option selected>Peter Jackson 4</option>
                    <option>Peter Jackson 5</option>
                </select>
            </div>
            <div class="form-group">
                <label>Actors</label>
                <select class="form-control" name="actor[]" multiple>
                    <option>Leonardo Di Caprio</option>
                    <option selected>Leonardo Di Caprio 2</option>
                    <option>Leonardo Di Caprio 3</option>
                    <option selected>Leonardo Di Caprio 4</option>
                    <option>Leonardo Di Caprio 5</option>
                </select>
            </div>
            <div class="form-group">
                <label>Edit Poster</label>
                <input type="text" class="form-control" name="poster"/>
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit"/>
            </div>
        </form>
    </div>

@endsection
