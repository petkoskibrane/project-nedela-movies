<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/')->name('show-movies')->uses('MovieController@showMovies');

Route::get('/showAddMovieForm')->name('add-movie-form')->uses('MovieController@showAddMovieForm');

Route::post('/addNewMovie')->name('add-movie')->uses('MovieController@addMovie');

Route::get('/details')->name('movie-details')->uses('MovieController@showDetails');

Route::delete('/delete')->name('delete')->uses('MovieController@deleteMovie');

Route::get('/editmovieform')->name('edit-movie-form')->uses('MovieController@showEditMovieForm');

Route::post('/edit')->name('edit-movie')->uses('MovieController@editMovie');

Route::post('/search')->name('search')->uses('MovieController@searchMovie');

Route::get('/search')->name('search')->uses('MovieController@searchMovieByYear');

Route::get('/actor')->name('actor-details')->uses('ActorController@showDetails');

Route::get('/director')->name('director-details')->uses('DirectorController@showDetails');


